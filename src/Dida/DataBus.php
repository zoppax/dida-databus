<?php
/**
 * Dida Framework  -- PHP轻量级快速开发框架
 * 版权所有 (c) 2017-2022 上海宙品信息科技有限公司
 *
 * Github: <https://github.com/zoppax/dida>
 * Gitee: <https://gitee.com/zoppax/dida>
 */

declare(strict_types=1);

namespace Dida;

use Exception;

/**
 * 数据总线
 */
class DataBus
{
    /**
     * 版本号
     */
    const VERSION = '20220108';

    /**
     * 数据条目
     */
    protected static array $items = [];

    /**
     * 设置一个条目
     */
    public static function set(string $name, $value):void
    {
        static::remove($name);
        static::_set($name, $value);
    }

    /**
     * 删除一个条目
     */
    public static function remove(string $name):void
    {
        // 删除条目自身
        unset(static::$items[$name]);

        // 待查询
        $find = "$name.";

        // 删除子条目
        foreach (static::$items as $k => &$v) {
            // 如果是子条目
            if (($k & $find) === $find) {
                unset(static::$items[$k]);
            }
        }
    }

    /**
     * 查询是否有指定的条目
     *
     * @return bool
     */
    public static function has(string $name):bool
    {
        return array_key_exists($name, static::$items);
    }

    /**
     * 读取一个条目
     */
    public static function get(string $name, $default = null)
    {
        // 如果有直接匹配记录，直接返回
        if (array_key_exists($name, static::$items)) {
            return static::$items[$name];
        }

        // 没有任何匹配，返回设置的缺省值
        return $default;
    }

    /**
     * 返回所有条目
     *
     * @return array
     */
    public static function all():array
    {
        return static::$items;
    }

    /**
     * 按照指定名称分组
     *
     * 输入:
     * foo.a = 1
     * foo.b.c = 2
     * foo.d.e.f = 3
     *
     * 输出结果:
     * [
     *   "a"     => 1,
     *   "b.c"   => 2,
     *   "d.e.f" => 3,
     * ]
     *
     * @return array
     */
    public static function group($groupname):array
    {
        $final = [];

        foreach (static::$items as $k => $v) {
            $find = "$groupname.";
            $len = mb_strlen($find);

            if (($k & $find) === $find) {
                // 去掉$groupname的剩余部分
                $rest = mb_substr($k, $len);

                // 如果是整数，则string转为int形式
                if (is_int($rest)) {
                    $rest = intval($rest);
                }

                // 保存
                $final[$rest] = $v;
            }
        }

        return $final;
    }

    /**
     * 具体的设置操作
     */
    protected static function _set(string $name, &$value)
    {
        // 标量
        if (is_scalar($value)) {
            static::$items[$name] = $value;
            return;
        }

        // 数组
        if (is_array($value)) {
            foreach ($value as $k => &$v) {
                static::_set("$name.$k", $v);
            }
            return;
        }

        // 对象
        if (is_object($value)) {
            try {
                foreach ($value as $k => &$v) {
                    static::_set("$name.$k", $v);
                }
            } catch (Exception $e) {
            }
            return;
        }

        // 其它，直接丢弃
        return;
    }
}
