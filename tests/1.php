<?php
/**
 * Dida Framework  -- PHP轻量级快速开发框架
 * 版权所有 (c) 2017-2022 上海宙品信息科技有限公司
 *
 * Github: <https://github.com/zoppax/dida>
 * Gitee: <https://gitee.com/zoppax/dida>
 */

use Dida\DataBus;

require __DIR__ . "/_common.php";

DataBus::set("foo.1", "a");
DataBus::set("foo.1.as", "b");
$group = DataBus::group("foo");
echo var_export($group);

DataBus::set("server", $_SERVER);
echo var_export(DataBus::all(), true);

echo "\n";

DataBus::set("http.get", $_GET);
DataBus::set("http.post", $_POST);
DataBus::set("http.header", apache_request_headers());
echo var_export(DataBus::all(), true);

echo DataBus::has("http.get") ? 1 : 0;

echo "\n";

$group = DataBus::group("http");
echo var_export($group);

echo "----\n";
// var_dump(array_diff($_SERVER,$group));
