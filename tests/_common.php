<?php
/**
 * Dida Framework  -- PHP轻量级快速开发框架
 * 版权所有 (c) 2017-2021 上海宙品信息科技有限公司
 *
 * Github: <https://github.com/zoppax/dida>
 * Gitee: <https://gitee.com/zoppax/dida>
 */

require __DIR__ . "/../vendor/autoload.php";
